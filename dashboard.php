<?php
    session_start();

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "tesztfeladat";

    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    if(isset($_POST['login']) && $_POST['login']==1)
    {
        $pass = hash('sha256', $_POST['password']);
        $sql = "SELECT * FROM employee WHERE email='".$_POST['email']."' AND password='".$pass."'";
        $result = $conn->query($sql);
        if($result->num_rows > 0)
        {
            $row = $result->fetch_assoc();
            $_SESSION['username'] = $row['name'];
            $_SESSION['userid'] = $row['id'];
        } else {
            $_SESSION['error'] = "Hibás felhasználónév vagy jelszó!";
            $_SESSION['flash'] = "1";
        }
    }
    if(isset($_GET['logout']) && $_GET['logout']==1)
    {
        unset($_SESSION['username']);
        unset($_SESSION['userid']);
        header('Location: index.php');
        $_SESSION['loggedout'] = 1;
    }
    if(isset($_SESSION['username']) && isset($_POST['newjob']) && $_POST['newjob']==1)
    {
        $conn->query("INSERT INTO job (name, description) VALUES ('".$_POST['name']."', '".$_POST['description']."')");
    }
    if(isset($_SESSION['username']) && isset($_POST['modifyjob']) && $_POST['modifyjob']==1)
    {
        $conn->query("UPDATE job SET name='".$_POST['name']."', description='".$_POST['description']."' WHERE id='".$_POST['jobid']."'");
    }
    if(isset($_SESSION['username']) && isset($_POST['deletejob']) && $_POST['deletejob']==1)
    {
        $conn->query("DELETE FROM job WHERE id='".$_POST['jobid']."'");
    }
    if(isset($_SESSION['username']) && isset($_POST['newuser']) && $_POST['newuser']==1)
    {
        if(isset($_POST['jobs']) && count($_POST['jobs']) > 0)
        {
            $conn->autocommit(FALSE);
            $conn->query("INSERT INTO employee (name, email, password, role) VALUES ('".$_POST['name']."', '".$_POST['email']."', '".hash("sha256", $_POST['password'])."', '0')");
            $last_id = mysqli_insert_id($conn);
            foreach($_POST['jobs'] as $us_job)
            {
                $conn->query("INSERT INTO employeejobs (employeeId, jobId) VALUES ('".$last_id."', '".$us_job."')");
            }
            $conn->commit();
            $conn->autocommit(TRUE);
        } else {
            $error = "Legalább 1 beosztást ki kell választani!";
        }
    }
    if(isset($_SESSION['username']) && isset($_POST['modifyuser']) && $_POST['modifyuser']==1)
    {
        $conn->autocommit(FALSE);
        $alljobs = array();
        $alljobquery = $conn->query("SELECT * FROM job");
        while($job = mysqli_fetch_assoc($alljobquery))
        {
            $alljobs[] = $job['id'];
        }
        $userjobsdb = array();
        $userjobquery = $conn->query("SELECT * FROM employeejobs WHERE employeeId='".$_POST['userid']."'");
        while($userjob = mysqli_fetch_assoc($userjobquery))
        {
            $userjobsdb[] = $userjob['jobId'];
        }
        $userjobs = array();
        if(isset($_POST['jobs']))
        {
            foreach($_POST['jobs'] as $jobs)
            {
                $userjobs[] = $jobs;
            }
        }
        if($_POST['password']=="")
        {
            
            $conn->query("UPDATE employee SET name='".$_POST['name']."', email='".$_POST['email']."' WHERE id='".$_POST['userid']."'");
        } else {
            $password = hash("sha256", $_POST['password']);
            $conn->query("UPDATE employee SET name='".$_POST['name']."', email='".$_POST['email']."', password='".$password."' WHERE id='".$_POST['userid']."'");
        }
        if(isset($_POST['jobs']) && count($_POST['jobs'])>0)
        {
            foreach($alljobs as $job)
            {
                if(in_array($job, $userjobs) && !in_array($job, $userjobsdb))
                {
                    $conn->query("INSERT INTO employeejobs (employeeId, jobId) VALUES ('".$_POST['userid']."', '".$job."')");
                } elseif(!in_array($job, $userjobs) && in_array($job, $userjobsdb))
                {
                    $conn->query("DELETE FROM employeejobs WHERE employeeId='".$_POST['userid']."' AND jobId='".$job."'");
                }
            }
        } else {
            $error = "Legalább 1 beosztásnak maradnia kell";
        }
        $conn->commit();
        $conn->autocommit(TRUE);
    }
    if(isset($_SESSION['username']) && isset($_POST['deleteuser']) && $_POST['deleteuser']==1)
    {
        $conn->autocommit(FALSE);
        $conn->query("DELETE FROM employee WHERE id='".$_POST['userid']."'");
        $conn->query("DELETE FROM employeejobs WHERE employeeId='".$_POST['userid']."'");
        $conn->commit();
        $conn->autocommit(TRUE);
    }
?> 
<?php if(empty($_SESSION['username'])) { 
    
    if(empty($_SESSION['error']) && !isset($_SESSION['loggedout']))
    {
        $_SESSION['error'] = "Az oldal megtekintéséhez jelentkezzen be!";
        $_SESSION['flash'] = "1";
    }
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="hu-HU">
    <head>
        <meta charset="utf-8">
        <title>Tesztfeladat</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/highcharts.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/highcharts.js"></script>
    </head>
    <body>
        
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2 col-sm-12 col-sm-offset-0 text-center">
                <h1>Bejelentkezve mint: <?php echo $_SESSION['username']; ?></h1><a href="dashboard.php?logout=1" class="btn btn-danger">Kijelentkezés</a>
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#user-list" aria-controls="user-list" role="tab" data-toggle="tab">Felhasználók</a></li>
                        <li role="presentation"><a href="#job-list" aria-controls="job-list" role="tab" data-toggle="tab">Munkakörök</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="user-list">
                            <div class="col-lg-12 search-div">
                                <form method="get" action="dashboard.php">
                                    <input type="text" class="search-input" placeholder="Név" name="name" />
                                    <input type="email" class="search-input" placeholder="E-mail cím" name="email" />
                                    <button type="submit" name="kereses" value="1" class="btn btn-warning">Keresés</button>
                                    <a href="dashboard.php" class="btn btn-success">Az összes felhasználó megjelenítése</a>
                                </form>
                            </div>
                            <button class="btn btn-primary new-button" type="button" data-toggle="collapse" data-target="#new-user" aria-expanded="false" aria-controls="new-user">
                                Új felhasználó hozzáadása
                            </button>
                            <h3 class="warning-text"><?php if(isset($error)) { echo $error; } ?></h3>
                            <div class="collapse" id="new-user">
                                <div class="col-lg-12">
                                    <form method="post" action="dashboard.php" enctype="multipart/form-data">
                                        <input type="email" name="email" placeholder="E-mail cím" />
                                        <input type="text" name="name" placeholder="Név" />
                                        <input type="password" name="password" placeholder="Jelszó" />
                                        <br>
                                        <?php $jobs = $conn->query("SELECT * FROM job");
                                            if($jobs->num_rows > 0) {
                                                while($job = mysqli_fetch_assoc($jobs)) { ?>
                                            <label for="<?php echo $job['id']; ?>"><?php echo $job['name']; ?></label>
                                            <input type="checkbox" name="jobs[]" id="<?php echo $job['id']; ?>" name="<?php echo $job['id']; ?>" value="<?php echo $job['id']; ?>" />
                                        <?php } } ?>
                                        <br>
                                        
                                        <button type="submit" class="btn btn-success" name="newuser" value="1">Hozzáadás</button>
                                    </form>
                                </div>
                            </div>
                            <?php 
                            if(isset($_GET['kereses']) && $_GET['kereses']==1)
                            {
                                $name = $conn->real_escape_string($_GET['name']);
                                $email = $conn->real_escape_string($_GET['email']);
                                $users = $conn->query("SELECT * FROM employee WHERE name='".$name."' OR email='".$email."'");
                            } else {
                                $users = $conn->query("SELECT * FROM employee"); 
                            }
                                        if($users->num_rows > 0) {
                                            while($user = mysqli_fetch_assoc($users)) { ?>
                                <div class="col-lg-12 userdiv">
                                    
                                    <form method="post" action="dashboard.php" enctype="multipart/form-data">
                                        <div class="col-lg-12">
                                            <input type="email" name="email" value="<?php echo $user['email']; ?>" placeholder="E-mail cím" />
                                            <input type="text" name="name" value="<?php echo $user['name']; ?>" placeholder="Név" />
                                            <input type="password" name="password" value="" placeholder="Jelszó" />
                                            <br>
                                            <?php 
                                            $userjobs = array();
                                            $userjobquery = $conn->query("SELECT * FROM employeejobs WHERE employeeId='".$user['id']."'");
                                            while($userjob = mysqli_fetch_assoc($userjobquery))
                                            {
                                                $userjobs[] = $userjob['jobId'];
                                            }
                                            $jobs = $conn->query("SELECT * FROM job");
                                                if($jobs->num_rows > 0) {
                                                    while($job = mysqli_fetch_assoc($jobs)) { ?>
                                                <label for="<?php echo $job['id']; ?>"><?php echo $job['name']; ?></label>
                                                <input type="checkbox" name="jobs[]" <?php if(in_array($job['id'], $userjobs)) { echo "checked"; } ?> id="<?php echo $job['id']; ?>" name="<?php echo $job['id']; ?>" value="<?php echo     $job['id']; ?>" />
                                            <?php } } ?>
                                            <br>
                                            <input type="hidden" name="userid" value="<?php echo $user['id']; ?>" />
                                        </div>
                                        <div class="col-lg-2 col-lg-offset-4">
                                            <button type="submit" class="btn btn-success" name="modifyuser" value="1">Módosítás</button>
                                        </div>
                                    </form>
                                    <div class="col-lg-2">
                                        <form method="post" action="dashboard.php" class="delete-form">
                                            <input type="hidden" name="userid" value="<?php echo $user['id']; ?>" />
                                            <button type="submit" name="deleteuser" value="1" class="btn btn-danger">Törlés</button>
                                        </form>
                                    </div>
                                </div>
                                <?php } } ?>
                            <div class="col-lg-12">
                                <div id="piechart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                    <script type="text/javascript">
                                        <?php $jobs = $conn->query("SELECT * FROM job");
                                            if($jobs->num_rows > 0) { ?>
                                        Highcharts.chart('piechart', {
                                            chart: {
                                             plotBackgroundColor: null,
                                             plotBorderWidth: null,
                                             plotShadow: false,
                                             type: 'pie'
                                            },
                                            title: {
                                                text: 'Munkakörök eloszlása'
                                            },
                                            tooltip: {
                                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                            },
                                            plotOptions: {
                                                pie: {
                                                    allowPointSelect: true,
                                                    cursor: 'pointer',
                                                    dataLabels: {
                                                        enabled: true,
                                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                        style: {
                                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                        }
                                                    }
                                                }
                                            },
                                            series: [{
                                                name: 'Munkakörök',
                                                colorByPoint: true,
                                                data: [
                                                <?php while($job = mysqli_fetch_assoc($jobs)) {  
                                                    $numberquery = $conn->query("SELECT COUNT(*) total FROM employeejobs WHERE jobId='".$job['id']."'");
                                                    $number = mysqli_fetch_assoc($numberquery);
                                                    ?>
                                                    {
                                                    name: "<?php echo $job['name']; ?>",
                                                    y: <?php echo $number['total']; ?>
                                                },
                                                    <?php } ?>
                                                    ]
                                         }]
                                    });
                                        <?php } ?>
		                          </script>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="job-list">
                            <button class="btn btn-primary new-button" type="button" data-toggle="collapse" data-target="#new-job" aria-expanded="false" aria-controls="new-job">
                                Új munkakör hozzáadása
                            </button>
                            <div class="collapse" id="new-job">
                                <div class="col-lg-12">
                                    <form method="post" action="dashboard.php">
                                        <div class="col-lg-12">
                                            <input type="text" name="name" class="new-input" placeholder="Munkakör neve" />
                                        </div>
                                        <div class="col-lg-12">
                                            <textarea name="description" class="new-input" rows="5" placeholder="Munkakör leírása"></textarea>
                                        </div>
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-success" name="newjob" value="1">Hozzáadás</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <?php $jobs = $conn->query("SELECT * FROM job");
                                if($jobs->num_rows > 0) { 
                                    while($job = mysqli_fetch_assoc($jobs)) { ?>
                            <div class="col-lg-12 userdiv">
                                <form method="post" action="dashboard.php">
                                    <div class="col-lg-12">
                                        <input type="text" name="name" class="new-input" placeholder="Munkakör neve" value="<?php echo $job['name']; ?>" />
                                    </div>
                                    <div class="col-lg-12">
                                        <textarea name="description" class="new-input" rows="5" placeholder="Munkakör leírása"><?php echo $job['description']; ?></textarea>
                                    </div>
                                    <input type="hidden" name="jobid" value="<?php echo $job['id']; ?>" />
                                    <div class="col-lg-2 col-lg-offset-4">
                                        <button type="submit" class="btn btn-success" name="modifyjob" value="1">Módosítás</button>
                                    </div>
                                </form>
                                <div class="col-lg-2">
                                    <form method="post" action="dashboard.php" class="delete-form">
                                        <input type="hidden" name="jobid" value="<?php echo $job['id']; ?>" />
                                        <button type="submit" name="deletejob" value="1" class="btn btn-danger">Törlés</button>
                                    </form>
                                </div>
                            </div>
                            <?php } } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>