<?php session_start(); 
    if(!empty($_SESSION['username']))
    {
        header('Location: dashboard.php');
    }
?>
<!DOCTYPE html>
<html lang="hu-HU">
    <head>
        <meta charset="utf-8">
        <title>Tesztfeladat</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/highcharts.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/highcharts.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="col-lg-4 col-lg-offset-4 col-sm-12 col-sm-offset-0 text-center">
                <h1>Bejelentkezés</h1>
                <form method="post" action="dashboard.php">
                    <input type="text" name="email" placeholder="E-mail cím" class="login-input" />
                    <input type="password" name="password" placeholder="Jelszó" class="login-input" />
                    <h3 class="warning-text"><?php if(isset($_SESSION['error'])) { echo $_SESSION['error']; } ?></h3>
                    <button type="submit" class="btn btn-primary w200" name="login" value="1">Bejelentkezés</button>
                </form>
            </div>
        </div>
    </body>
</html>
<?php if(isset($_SESSION['flash']) && $_SESSION['flash']==1) { $_SESSION['error']=""; } 
if(isset($_SESSION['loggedout']) && $_SESSION['loggedout']==1)
{
    unset($_SESSION['loggedout']);
}
?>